import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  //create user object based on FirebaseUser

  /* User _userFromFirebaseUser(User? user) {
    return user != null ? User(uid: user.uid) : null;
    
  }*/
  
   BaseUser? _userfromFirebase(User? user) {
    return user != null ? BaseUser(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<BaseUser?> get user {
    return _auth
        .authStateChanges()
        .map(_userfromFirebase);
  }


  //sign in anon

  Future signInAnon() async {
    try {
      UserCredential result = await _auth.signInAnonymously();
      User? user = result.user;
      return user;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //sign in with email and password

  //regsiter

  //sign out

}

class BaseUser 
{
   late final String? uid;

  BaseUser({this.uid});
}


