import 'package:flutter/material.dart';
import 'package:flutter_pets/start.dart';

import 'home.dart';

//void main() => runApp(MyApp());

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/Screen.png'),
                  fit: BoxFit.fill,
                )),
            padding: EdgeInsets.all(40),
            child: Column(children: [
              SizedBox(
                height: 100,
              ),
              Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    child: Text(
                      "Registro",
                      style: TextStyle(color: Colors.black, fontSize: 60),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Nombre',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  Container(
                    decoration:
                    BoxDecoration(border: Border.all(color: Colors.black)),
                    child: TextField(
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.people_alt_outlined),
                        hintStyle: TextStyle(color: Colors.black),
                        hintText: " Ingrese su nombre...",
                        border: OutlineInputBorder(),
                      ),
                      keyboardType: TextInputType.emailAddress,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Email',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                  ),
                  Container(
                    decoration:
                    BoxDecoration(border: Border.all(color: Colors.black)),
                    child: TextField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.email_outlined),
                          hintStyle: TextStyle(color: Colors.black),
                          hintText: " Ingrese su correo electronico...",
                          border: OutlineInputBorder()),
                      keyboardType: TextInputType.emailAddress,
                    ),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Contraseña',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, height: 2, fontSize: 20),
                    ),
                  ),
                  Container(
                    decoration:
                    BoxDecoration(border: Border.all(color: Colors.black)),
                    child: TextField(
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.password_outlined),
                        hintStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(),
                        hintText: " Ingrese la contraseña...",
                      ),
                      obscureText: true,
                      enableSuggestions: false,
                      autocorrect: false,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              ElevatedButton(
                child: Text('Registrarse'),
                style: ElevatedButton.styleFrom(
                    padding:
                    const EdgeInsets.symmetric(horizontal: 65, vertical: 22),
                    primary: Colors.yellow,
                    onPrimary: Colors.black,
                    onSurface: Colors.grey,
                    textStyle: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold)),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Home()),
                  );
                },
              ),
              const SizedBox(
                height: 15,
              ),
              ElevatedButton(
                child: Text('Regresar'),
                style: ElevatedButton.styleFrom(
                    padding:
                    const EdgeInsets.symmetric(horizontal: 78, vertical: 22),
                    primary: Colors.blue,
                    onPrimary: Colors.white,
                    onSurface: Colors.grey,
                    textStyle: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold)),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Start()),
                  );
                },
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
