import 'dart:async';
import 'package:flutter/foundation.dart' show ValueNotifier;
import 'local_storage/directory/directory.dart';

class LocalStorage {
  Stream<Map<String, dynamic>> get stream => _dir.stream;
  Map<String, dynamic>? _initialData;

  static final Map<String, LocalStorage> _cache = new Map();

  late DirUtils _dir;
  late DirUtils _dirVets;
  ValueNotifier<Error> onError = ValueNotifier(Error());
  late Future<bool> ready;

  factory LocalStorage(String key, [String? path, Map<String, dynamic>? initialData]) {
    if (_cache.containsKey(key)) {
      return _cache[key]!;
    } else {
      final instance = LocalStorage._internal(key, path, initialData);
      _cache[key] = instance;
      return instance;
    }
  }

  void dispose() {
    _dir.dispose();
  }

  LocalStorage._internal(String key, [String? path, Map<String, dynamic>? initialData]) {
    _dir = DirUtils(key, path);
    _initialData = initialData;

    ready = new Future<bool>(() async {
      await this._init();
      return true;
    });
  }

  Future<void> _init() async {
    try {
      await _dir.init(_initialData ?? {});
    } on Error catch (err) {
      onError.value = err;
    }
  }

  dynamic getAppoinment(String key) {
    return _dir.getItem(key);
  }

  Future<void> setAppoinment(
      String key,
      value, [
        Object toEncodable(Object nonEncodable)?,
      ]) async {
    var data = toEncodable?.call(value) ?? null;
    if (data == null) {
      try {
        data = value.toJson();
      } on NoSuchMethodError catch (_) {
        data = value;
      }
    }

    await _dir.setItem(key, data);
    return _flush();
  }

  Future<void> deleteAppoinment(String key) async {
    await _dir.remove(key);
    return _flush();
  }

  Future<void> clear() async {
    await _dir.clear();
    return _flush();
  }

////////////////////////////
//////////////////////////// VETS
////////////////////////////


  dynamic getVetAppoinment(String key)
  {
    return _dirVets.getItem(key);
  }

  Future<void> setVetAppoinment(
      String key,
      value, [
        Object toEncodable(Object nonEncodable)?,
      ]) async
  {
    var data = toEncodable?.call(value) ?? null;
    if (data == null)
    {
      try {
        data = value.toJson();
      } on NoSuchMethodError catch (_)
      {
        data = value;
      }
    }
    await _dirVets.setItem(key, data);
    return _flush();
  }

  Future<void> deleteVetAppoinment(String key) async {
    await _dirVets.remove(key);
    return _flush();
  }

  Future<void> _flush() async {
    try {
      await _dir.flush();
    } catch (e) {
      rethrow;
    }
    return;
  }
}