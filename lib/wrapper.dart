import 'package:flutter/material.dart';
import 'package:flutter_pets/home.dart';
import 'package:flutter_pets/services/auth.dart';
import 'package:flutter_pets/start.dart';
import 'package:flutter_pets/login_config/user.dart';

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //Return either Home or Start widget
    return Start();
  }
}
