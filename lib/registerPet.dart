import 'package:flutter/material.dart';

import 'home.dart';

void main() {
  runApp(registerPet());
}

class registerPet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: PetForm(),
        ),
      ),
    );
  }
}

class PetForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterPet();
  }
}

class RegisterPet extends State<PetForm> {
  String _imagen = "";
  String _nombre = "";
  String _raza = "";
  String _edad = "";
  String _peso = "";
  String _comidaFav = "";
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildImagen() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Imagen"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La imagen es requerida";
        }
        return null;
      },
      onSaved: (value) {
        _imagen = value!;
      },
    );
  }

  Widget _buildNombre() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Nombre"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El nombre es requerido";
        }
        return null;
      },
      onSaved: (value) {
        _nombre = value!;
      },
    );
  }

  Widget _buildRaza() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Raza"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La raza es requerida";
        }
        return null;
      },
      onSaved: (value) {
        _raza = value!;
      },
    );
  }

  Widget _buildEdad() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Edad"),
      keyboardType: TextInputType.number,
      validator: (value) {
        int? edad = int.tryParse(value!);
        if (value == null || value.isEmpty) {
          return "La imagen es requerida";
        } else if (edad! < 0 || edad > 30) {
          return "La edad no es valida";
        }
        return null;
      },
      onSaved: (value) {
        _edad = value!;
      },
    );
  }

  Widget _buildPeso() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Peso"),
      keyboardType: TextInputType.number,
      validator: (value) {
        int? peso = int.tryParse(value!);
        if (value == null || value.isEmpty) {
          return "El peso es requerido";
        } else if (peso! < 0 || peso > 100) {
          return "El peso no es valido";
        }
        return null;
      },
      onSaved: (value) {
        _peso = value!;
      },
    );
  }

  Widget _buildComidaFav() {
    return TextFormField(
      decoration: InputDecoration(labelText: "Comida favorita"),
      onSaved: (value) {
        _comidaFav = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mascotas'),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage('assets/images/Screen.png'),
            fit: BoxFit.fill,
          )),
          padding: EdgeInsets.only(top: 20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: new EdgeInsets.symmetric(horizontal: 20.0),
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                    child: Text('Regresar'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Home()),
                      );
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.blue),
                      textStyle: MaterialStateProperty.all(
                        TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Añadir mascota",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 38.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          _buildImagen(),
                          _buildNombre(),
                          _buildRaza(),
                          _buildEdad(),
                          _buildPeso(),
                          _buildComidaFav(),
                          SizedBox(
                            height: 50,
                          ),
                          FloatingActionButton(
                            child: Text("Añadir"),
                            onPressed: () => {
                              if (_formKey.currentState!.validate())
                                {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text('Processing Data')),
                                  )
                                }
                              else
                                {
                                  print(_imagen),
                                  print(_nombre),
                                  print(_raza),
                                  print(_edad),
                                  print(_peso),
                                  print(_comidaFav),
                                  _formKey.currentState!.save()
                                }
                            },
                          )
                        ],
                      )),
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doge50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/man50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/cart48.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
            label: '',
          ),
        ],
      ),
    );
  }
}
