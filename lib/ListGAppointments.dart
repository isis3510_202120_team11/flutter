import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
// Import the firebase_core plugin
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_pets/CAppointmentG.dart';
import 'package:geolocator/geolocator.dart';
import 'home.dart';

//void main() => runApp(MyApp());

class LGAppointments extends StatefulWidget {
  @override
  _LGAState createState() => _LGAState();
}

class _LGAState extends State<LGAppointments> {
  bool _initialized = false;
  bool _error = false;

  void initGroomers() {
    if (_initialized) {
      grooms = FirebaseFirestore.instance.collection("GroomAppointment");
    }
  }

  // Define an async function to initialize FlutterFire
  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
        initGroomers();
      });
    } catch (e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }

  static final customCacheManager = CacheManager(
    Config(
      'customCacheKey',
      stalePeriod: Duration(days: 1),
      maxNrOfCacheObjects: 10,
    ),
  );

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  late CollectionReference grooms;

  @override
  Widget build(BuildContext context) {
    // Show error message if initialization failed
    if (_error) {
      return MaterialApp(
        home: Scaffold(
          body: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/Screen.png'),
                )),
            padding: EdgeInsets.only(top: 50.0),
            child: Column(
              children: [
                Container(
                  margin: new EdgeInsets.symmetric(horizontal: 20.0),
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                    child: Text('Regresar'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Home()),
                      );
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.blue),
                      textStyle: MaterialStateProperty.all(
                        TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                        flex: 3,
                        child: Center(
                          child: Image.asset('assets/images/man100.png'),
                        )),
                    Expanded(
                      flex: 4,
                      child: Text(
                        "Adopt",
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.8),
                            fontSize: 38.0,
                            height: 1.4,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Column(
                    children: [
                      Text("Error!")
                    ],
                  ),
                )
              ],
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/doge50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/man50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/cart48.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
                label: '',
              ),
            ],
          ),
        ),
      );
    }

    // Show a loader until FlutterFire is initialized
    if (!_initialized) {
      return MaterialApp(
        home: Scaffold(
          body: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/Screen.png'),
                )),
            padding: EdgeInsets.only(top: 50.0),
            child: Column(
              children: [
                Container(
                  margin: new EdgeInsets.symmetric(horizontal: 20.0),
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                    child: Text('Regresar'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Home()),
                      );
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.blue),
                      textStyle: MaterialStateProperty.all(
                        TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                        flex: 3,
                        child: Center(
                          child: Image.asset('assets/images/man100.png'),
                        )),
                    Expanded(
                      flex: 4,
                      child: Text(
                        "Adopt",
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.8),
                            fontSize: 38.0,
                            height: 1.4,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Column(
                    children: [
                      Text("Cargando!")
                    ],
                  ),
                )
              ],
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/doge50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/man50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/cart48.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
                label: '',
              ),
            ],
          ),
        ),
      );
    }
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage('assets/images/Screen.png'),
          )),
          padding: EdgeInsets.only(top: 50.0),
          child: Column(
            children: [
              Container(
                margin: new EdgeInsets.symmetric(horizontal: 20.0),
                alignment: Alignment.topRight,
                child: ElevatedButton(
                  child: Text('Regresar'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.blue),
                    textStyle: MaterialStateProperty.all(
                      TextStyle(fontSize: 15),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      flex: 3,
                      child: Center(
                        child: Image.asset('assets/images/soap100.png'),
                      )),
                  Expanded(
                    flex: 4,
                    child: Text(
                      "Grooming",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 38.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(40.0),
                child: Column(
                  children: [
                    FutureBuilder(
                      future: grooms.get(),
                      builder: (_, AsyncSnapshot<dynamic> snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Center(
                            child: Text("Loading..."),
                          );
                        } else if (snapshot.connectionState ==
                            ConnectionState.none) {
                          return Center(
                            child: Text("Not connected"),
                          );
                        } else {
                          return ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: snapshot.data.docs.length,
                              itemBuilder: (_, index) {
                                //print(distanceInKm);
                                {
                                  return Row(
                                    children: [
                                      Expanded(
                                        flex: 3,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            CachedNetworkImage(
                                              key: UniqueKey(),
                                              cacheManager: customCacheManager,
                                              imageUrl: snapshot
                                                  .data.docs[index]["img"],
                                              placeholder: (context, url) =>
                                                  const CircularProgressIndicator(),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Container(
                                                color: Colors.black12,
                                                child: Icon(Icons.error,
                                                    color: Colors.red),
                                              ),
                                            ),
                                            Container(
                                              margin: new EdgeInsets.symmetric(
                                                  horizontal: 20.0),
                                              alignment: Alignment.topRight,
                                              child: ElevatedButton(
                                                child: Text('Regresar'),
                                                onPressed: () {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          (Home()),
                                                    ),
                                                  );
                                                },
                                                style: ButtonStyle(
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          Colors.blue),
                                                  textStyle:
                                                      MaterialStateProperty.all(
                                                    TextStyle(fontSize: 15),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    "Proveedor:",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: Text(snapshot.data
                                                      .docs[index]["provider"]),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 7,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    "Servicio:",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: Text(snapshot.data
                                                      .docs[index]["service"]
                                                      .toString()),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 7,
                                            ),
                                            Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    "Costo:",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: Text(snapshot
                                                      .data.docs[index]["price"]
                                                      .toString()),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 7,
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 50,
                                      ),
                                    ],
                                  );
                                }
                              });
                        }
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doge50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/man50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/cart48.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
              label: '',
            ),
          ],
        ),
      ),
    );
  }
}
