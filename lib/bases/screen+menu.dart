import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        body: Center(child: Image.asset('assets/images/Screen.png')),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doge50.png')),
              label: 'Mascotas',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/man50.png')),
              label: 'Perfil',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
              label: 'Inicio',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/cart48.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
              label: '',
            ),
          ],
        ),
      ),
    );
  }
}

