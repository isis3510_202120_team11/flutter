import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'home.dart';


class AdoptarForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Adoptar();
  }
}

class Adoptar extends State<StatefulWidget> {
  String _breed = '';
  String _client = '';
  String _correo = '';
  String _celular = '';
  String _localidad = '';
  String _fecha = '';
  String _petName = '';


  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final myController = TextEditingController();
  final myController1 = TextEditingController();
  final myController2 = TextEditingController();
  final myController3 = TextEditingController();
  final myController4 = TextEditingController();
  final myController5 = TextEditingController();
  final myController6 = TextEditingController();

  void dispose() {
    //Clean up the controller when the widget is disposed.

    myController.dispose();
    super.dispose();
  }

  Widget _wClient() {
    return TextFormField(
      controller: myController,
      decoration: InputDecoration(labelText: "Nombre del usuario"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El nombre es requerido";
        }
        return null;
      },
      onChanged: (value) {
        _client = myController.text;
      },
    );
  }

  Widget _wCorreo() {
    return TextFormField(
      controller: myController1,
      decoration: InputDecoration(labelText: "Correo del usuario"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El Correo es requerido";
        }
        return null;
      },
      onChanged: (value) {
        _client = myController1.text;
      },
    );
  }

  Widget _wCelular() {
    return TextFormField(
      controller: myController2,
      decoration: InputDecoration(labelText: "Celular del usuario"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El Celular es requerido";
        }
        return null;
      },
      onChanged: (value) {
        _client = myController2.text;
      },
    );
  }

  Widget _wLocalidad() {
    return TextFormField(
      controller: myController3,
      decoration: InputDecoration(labelText: "Localidad del usuario"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La localidad del usuario es requerida";
        }
        return null;
      },
      onChanged: (value) {
        _client = myController3.text;
      },
    );
  }

  Widget _wFecha() {
    return TextFormField(
      controller: myController4,
      decoration: InputDecoration(labelText: "Fecha"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La fecha es requerida";
        }
        return null;
      },
      onChanged: (value) {
        _fecha = myController4.text;
      },
    );
  }

  Widget _wPetName() {
    return TextFormField(
      controller: myController5,
      decoration: InputDecoration(labelText: "Nombre mascota"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El nombre de la mascota es requerido";
        }
        return null;
      },
      onChanged: (value) {
        _petName = myController5.text;
      },
    );
  }

  Widget _wBreed() {
    return TextFormField(
      controller: myController6,
      decoration: InputDecoration(labelText: "Raza"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La raza del perro es requerida";
        }
        return null;
      },
      onChanged: (value) {
        _breed = myController6.text;
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Adoptar'),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/Screen.png'),
                fit: BoxFit.fill,
              )),
          padding: EdgeInsets.only(top: 20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: new EdgeInsets.symmetric(horizontal: 20.0),
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                    child: Text('Regresar'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Home()),
                      );
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.blue),
                      textStyle: MaterialStateProperty.all(
                        TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Adoptar mascota",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 38.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          _wClient(),
                          _wCorreo(),
                          _wCelular(),
                          _wLocalidad(),
                          _wFecha(),
                          _wPetName(),
                          _wBreed(),
                          SizedBox(
                            height: 50,
                          ),
                          FloatingActionButton(
                              child: Text("Añadir"),
                              onPressed: () => [
                                FirebaseFirestore.instance
                                    .collection('Appointment')
                                    .add(
                                  {
                                    'client': _client,
                                    'celular': _celular,
                                    'correo': _correo,
                                    'localidad': _localidad,
                                    'fecha': _fecha,
                                    'petName': _petName,
                                    'breed': _breed,
                                    //Navigator.push(context, MaterialPageRoute(builder:(context) => LGAppointments,))}
                                  },
                                ),
                              ]

                            /*
                              if (_formKey.currentState!.validate())
                                {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text('Processing Data')),
                                  ),
                                }
                              else
                                {
                                  print(_client),
                                  print(_fecha),
                                  print(_hora),
                                  print(_petName),
                                  print(_price),
                                  print(_walker),
                                  print(_walkTime),
                                  _formKey.currentState!.save()
                                }
                            },*/
                          )
                        ],
                      )),
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doge50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/man50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/cart48.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
            label: '',
          ),
        ],
      ),
    );
  }
}
