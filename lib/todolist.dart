import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'localstorage.dart';

class Todolist extends StatefulWidget {
  Todolist({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}


class TodoItem {
  String title;
  bool done;

  TodoItem({required this.title, required this.done});

  toJSONEncodable() {
    Map<String, dynamic> m = new Map();

    m['title'] = title;
    m['done'] = done;

    return m;
  }
}

class TodoList {
  List<TodoItem> items = [];

  toJSONEncodable() {
    return items.map((item) {
      return item.toJSONEncodable();
    }).toList();
  }
}


class _MyHomePageState extends State {
  final TodoList list = new TodoList();
  final LocalStorage storage = new LocalStorage('todo_app');
  bool initialized = false;
  TextEditingController controller = new TextEditingController();

  _toggleItem(TodoItem item) {
    setState(() {
      item.done = !item.done;
      _saveToStorage();
    });
  }

  _addItem(String title) {
    setState(() {
      final item = new TodoItem(title: title, done: false);
      list.items.add(item);
      _saveToStorage();
    });
  }

  _saveToStorage() {
    storage.setAppoinment('todos', list.toJSONEncodable());
  }

  _clearStorage() async {
    await storage.clear();

    setState(() {
      list.items = storage.getAppoinment('todos') ?? [];
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: const Text('Lista de tareas'),
      ),
      body: Container(
          padding: EdgeInsets.all(25.0),
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/Screen.png'),
                fit: BoxFit.fill,
              )),
          constraints: BoxConstraints.expand(),

          child: FutureBuilder(
            future: storage.ready,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.data == null)
              {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
              }

              if (!initialized) {
                var items = storage.getAppoinment('todos');

                if (items != null) {
                  list.items = List<TodoItem>.from(
                    (items as List).map(
                          (item) => TodoItem(
                        title: item['title'],
                        done: item['done'],
                      ),
                    ),
                  );
                }

                initialized = true;
              }

              List<Widget> widgets = list.items.map((item) {
                return CheckboxListTile(
                  value: item.done,
                  autofocus: false,
                  activeColor: Colors.green,
                  checkColor: Colors.white,
                  title: Text(item.title),
                  selected: item.done,
                  onChanged: (_) {
                    _toggleItem(item);
                  },
                  secondary: const Icon(Icons.add_rounded),
                );
              }).toList();

              return Column(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: ListView(
                      children: widgets,
                      itemExtent: 40.0,
                    ),
                  ),
                  ListTile(
                    title: TextField(
                      controller: controller,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.task_alt_outlined),
                        labelText: ' Escriba la tarea a realizar...',
                        border: OutlineInputBorder(),
                        fillColor: Colors.black
                      ),
                      onEditingComplete: _save,
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                        IconButton(
                        icon: Icon(Icons.save),
                        iconSize: 35,
                        alignment: Alignment.centerRight,
                        color: Colors.black,
                        onPressed: _save,
                        tooltip: 'Save',
                    ),
                        IconButton(
                            icon: Icon(Icons.delete),
                          iconSize: 35,
                          alignment: Alignment.centerRight,
                          color: Colors.blue,
                          onPressed: _clearStorage,
                          tooltip: 'Clear storage',
                        )
                ],
              ),
                  SizedBox(height: 20,),
                ],
              );
            },
          )),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doge50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/man50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/cart48.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
            label: '',
          ),
        ],
      ),
    );
  }

  void _save() {
    _addItem(controller.value.text);
    controller.clear();
  }
}