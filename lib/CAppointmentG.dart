import 'package:flutter/material.dart';
import 'package:flutter_pets/walker.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pets/walker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'ListGAppointments.dart';
import 'home.dart';
import 'login_config/utils.dart';

import 'home.dart';
import 'login_config/utils.dart';

class GAppointments extends StatefulWidget {
  @override
  _GAppointmentsState createState() => _GAppointmentsState();
}

class _GAppointmentsState extends State<GAppointments> {
  late final ValueNotifier<List<Event>> _selectedEvents;
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  DateTime? _rangeStart;
  DateTime? _rangeEnd;

  @override
  void initState() {
    super.initState();

    _selectedDay = _focusedDay;
    _selectedEvents = ValueNotifier(_getEventsForDay(_selectedDay!));
  }

  @override
  void dispose() {
    _selectedEvents.dispose();
    super.dispose();
  }

  List<Event> _getEventsForDay(DateTime day) {
    // Implementation example
    return kEvents[day] ?? [];
  }

  void _onDaySelected(DateTime selectedDay, DateTime focusedDay) {
    if (!isSameDay(_selectedDay, selectedDay)) {
      setState(() {
        _selectedDay = selectedDay;
        _focusedDay = focusedDay;
      });

      _selectedEvents.value = _getEventsForDay(selectedDay);
    }
  }

  @override
  Widget build(BuildContext context) {
    initializeDateFormatting();
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(height: 80.0),
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 20.0),
            alignment: Alignment.topRight,
            child: ElevatedButton(
              child: Text('Regresar'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Home()),
                );
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.blue),
                textStyle: MaterialStateProperty.all(
                  TextStyle(fontSize: 15),
                ),
              ),
            ),
          ),
          Text(
            "Agenda",
            textAlign: TextAlign.center,
            textScaleFactor: 3.0,
          ),
          TableCalendar<Event>(
            locale: 'es_ES',
            firstDay: kFirstDay,
            lastDay: kLastDay,
            focusedDay: _focusedDay,
            selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
            rangeStartDay: _rangeStart,
            rangeEndDay: _rangeEnd,
            calendarFormat: _calendarFormat,
            eventLoader: _getEventsForDay,
            startingDayOfWeek: StartingDayOfWeek.monday,
            calendarStyle: CalendarStyle(
              outsideDaysVisible: false,
            ),
            onDaySelected: _onDaySelected,
            onFormatChanged: (format) {
              if (_calendarFormat != format) {
                setState(() {
                  _calendarFormat = format;
                });
              }
            },
            onPageChanged: (focusedDay) {
              _focusedDay = focusedDay;
            },
          ),
          const SizedBox(height: 8.0),
          Expanded(
            child: ValueListenableBuilder<List<Event>>(
              valueListenable: _selectedEvents,
              builder: (context, value, _) {
                return ListView.builder(
                  itemCount: value.length,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: const EdgeInsets.symmetric(
                        horizontal: 12.0,
                        vertical: 4.0,
                      ),
                      decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      child: ListTile(
                        onTap: () {
                          AlertDialog alert = AlertDialog(
                            title: Text('Su cita fue agendada con exito'),
                          );
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return alert;
                              });
                        },
                        title: Text('${value[index]}'),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doge50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/man50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/cart48.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
            label: '',
          ),
        ],
      ),
    );
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    print(directory.path);
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/data.txt');
  }

  Future<File> writeContent() async {
    final file = await _localFile;
    //Write file instructions
    return file.writeAsString('Hello folks');
  }

  Future<String> readContent() async {
    try {
      final file = await _localFile;
      //Read the file
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      //If there is an error reading, return a default String;
      return 'Error';
    }
  }

//Start here
}

class AgendaFormG extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterAgendaG();
  }
}

class RegisterAgendaG extends State<StatefulWidget> {
  String _breed = '';
  String _client = '';
  String _fecha = '';
  String _hora = '';
  String _petName = '';
  int _price = 0;
  String _provider = '';
  String _service = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final myController = TextEditingController();
  final myController1 = TextEditingController();
  final myController2 = TextEditingController();
  final myController3 = TextEditingController();
  final myController4 = TextEditingController();
  final myController5 = TextEditingController();
  final myController6 = TextEditingController();
  final myController7 = TextEditingController();

  void dispose() {
    //Clean up the controller when the widget is disposed.

    myController.dispose();
    super.dispose();
  }

  Widget _wClient() {
    return TextFormField(
      controller: myController,
      decoration: InputDecoration(labelText: "Nombre del usuario"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El nombre es requerido";
        }
        return null;
      },
      onChanged: (value) {
        _client = myController.text;
      },
    );
  }

  Widget _wFecha() {
    return TextFormField(
      controller: myController1,
      decoration: InputDecoration(labelText: "Fecha"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La fecha es requerida";
        }
        return null;
      },
      onChanged: (value) {
        _fecha = myController1.text;
      },
    );
  }

  Widget _wHora() {
    return TextFormField(
      controller: myController2,
      decoration: InputDecoration(labelText: "Hora"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La hora es requerida";
        }
        return null;
      },
      onChanged: (value) {
        _hora = myController2.text;
      },
    );
  }

  Widget _wPetName() {
    return TextFormField(
      controller: myController3,
      decoration: InputDecoration(labelText: "Nombre mascota"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El nombre de la mascota es requerido";
        }
        return null;
      },
      onChanged: (value) {
        _petName = myController3.text;
      },
    );
  }

  Widget _wPrice() {
    return TextFormField(
      controller: myController4,
      decoration: InputDecoration(labelText: "Precio"),
      keyboardType: TextInputType.number,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El precio es requerido";
        }
        return null;
      },
      onChanged: (value) {
        int peso = int.parse(myController4.text);
        _price = peso;
      },
    );
  }

  Widget _wProvider() {
    return TextFormField(
      controller: myController5,
      decoration: InputDecoration(labelText: "Proveedor"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El nombre del proveedor es requerido";
        }
        return null;
      },
      onChanged: (value) {
        _provider = myController5.text;
      },
    );
  }

  Widget _wBreed() {
    return TextFormField(
      controller: myController6,
      decoration: InputDecoration(labelText: "Raza"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La raza del perro es requerida";
        }
        return null;
      },
      onChanged: (value) {
        _breed = myController6.text;
      },
    );
  }

  Widget _wService() {
    return TextFormField(
      controller: myController7,
      decoration: InputDecoration(labelText: "Servicio"),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El servicio es requerida";
        }
        return null;
      },
      onChanged: (value) {
        _breed = myController7.text;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mascotas'),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage('assets/images/Screen.png'),
            fit: BoxFit.fill,
          )),
          padding: EdgeInsets.only(top: 20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: new EdgeInsets.symmetric(horizontal: 20.0),
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                    child: Text('Regresar'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Home()),
                      );
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.blue),
                      textStyle: MaterialStateProperty.all(
                        TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Añadir mascota",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 38.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          _wClient(),
                          _wFecha(),
                          _wHora(),
                          _wPetName(),
                          _wPrice(),
                          _wProvider(),
                          _wService(),
                          _wBreed(),
                          SizedBox(
                            height: 50,
                          ),
                          FloatingActionButton(
                              child: Text("Añadir"),
                              onPressed: () => [
                                    FirebaseFirestore.instance
                                        .collection('Appointment')
                                        .add(
                                      {
                                        'client': _client,
                                        'fecha': _fecha,
                                        'hora': _hora,
                                        'petName': _petName,
                                        'price': _price,
                                        'provider': _provider,
                                        'service': _service,
                                        'breed': _breed,
                                        //Navigator.push(context, MaterialPageRoute(builder:(context) => LGAppointments,))}
                                      },
                                    ),
                                  ]

                              /*
                              if (_formKey.currentState!.validate())
                                {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                        content: Text('Processing Data')),
                                  ),
                                }
                              else
                                {
                                  print(_client),
                                  print(_fecha),
                                  print(_hora),
                                  print(_petName),
                                  print(_price),
                                  print(_walker),
                                  print(_walkTime),
                                  _formKey.currentState!.save()
                                }
                            },*/
                              ),
                          FloatingActionButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LGAppointments()),
                              );
                            },
                            child: Text('Ver citas'),
                          )
                        ],
                      )),
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doge50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/man50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/cart48.png')),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
            label: '',
          ),
        ],
      ),
    );
  }
}
