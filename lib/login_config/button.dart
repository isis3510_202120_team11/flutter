import 'dart:ui';

import 'package:flutter/material.dart';

import '../home.dart';
import '../schedule.dart';
import '../start.dart';

class Button extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return Container(
        alignment: Alignment.topRight,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(100),
        ),
        child: ButtonTheme(
          child:  Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              ElevatedButton(
                child: Text('Login'),
                style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                    primary: Colors.yellow,
                    onPrimary: Colors.black,
                    onSurface: Colors.grey,
                    textStyle:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Home()),
                  );},),
              SizedBox(height: 30,),
              ElevatedButton(
                child: Text('Regresar'),
                style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                    primary: Colors.blue,
                    onPrimary: Colors.black,
                    onSurface: Colors.grey,
                    textStyle:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Start()),
                  );},),
            ],
          ),
        )
    );
  }
}