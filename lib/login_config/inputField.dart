import 'package:flutter/material.dart';

class InputField extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.topLeft,
          child: Text(
            'Email',
            textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.black
              )
          ),
          child: TextField(
            decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.black),
                hintText: "  ejemplo@gmail.com",
                border: InputBorder.none
            ),
            keyboardType: TextInputType.emailAddress,
          ),
        ),
        Container(
          alignment: Alignment.topLeft,
          child: Text(
            'Contraseña',
            textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontWeight: FontWeight.bold,height: 2, fontSize: 20),),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.all(
                  color: Colors.black
              )
          ),
          child: TextField(
            decoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.black),
                border: InputBorder.none,
                hintText: "  *********",
            ),
            obscureText: true,
            enableSuggestions: false,
            autocorrect: false,
          ),
        ),
      ],
    );
  }
}