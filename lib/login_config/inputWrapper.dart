import 'package:flutter/material.dart';

import 'button.dart';
import 'inputField.dart';

class InputWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Colors.white,

            ),
            child: InputField(),
          ),
          Text(
            "",
            style: TextStyle(color: Colors.black, fontSize: 16),
          ),
          SizedBox(height: 20,),
          Button()
        ],
      ),
    );
  }
}