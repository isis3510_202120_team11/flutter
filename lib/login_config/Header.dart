import 'package:flutter/material.dart';

class Header extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: new EdgeInsets.symmetric(horizontal: 25.0),
            alignment: Alignment.topLeft,
            child: Text("Login", style: TextStyle(color: Colors.black, fontSize: 60),),
          ),
          SizedBox(height: 10,),
        ],
      ),
    );
  }
}