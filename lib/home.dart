import 'package:flutter/material.dart';
import 'package:flutter_pets/registerPet.dart';
import 'package:flutter_pets/schedule.dart';
import 'package:flutter_pets/start.dart';
import 'package:flutter_pets/vets.dart';
import 'package:flutter_pets/walker.dart';
import 'package:flutter_pets/todolist.dart';

import 'ListAppoinment.dart';
import 'adoptionPets.dart';
import 'grooming.dart';

//void main() => runApp(MyApp());

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          constraints: BoxConstraints.expand(),
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage('assets/images/Screen.png'),
            fit: BoxFit.fill,

          )),
          padding: EdgeInsets.only(top: 30.0),
          child: Column(
            children: [
              SizedBox(height: 20,),
              Container(
                margin: new EdgeInsets.symmetric(horizontal: 45.0),
                alignment: Alignment.topRight,
                child:
                ElevatedButton(
                  child: Text('Cerrar Sesion'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Start()),
                    );
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.blue),
                    textStyle: MaterialStateProperty.all(
                      TextStyle(fontSize: 15),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Row(
                children: [
                  Expanded(
                    flex: 4,
                    child: Text(
                      "Home",
                      style: TextStyle(
                          color: Colors.blue.withOpacity(0.8),
                          fontSize: 38.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
              SizedBox(height: 20,),
              Row(
                children: [
                  Expanded(
                      flex: 3,
                      child: Center(
                          child: IconButton(
                        icon: Image.asset('assets/images/doge50.png'),
                        iconSize: 50,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => registerPet()),
                          );
                        },
                      ))),
                  Expanded(
                    flex: 4,
                    child: Text(
                      "Mascotas",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 30.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  ),
                   ],
              ),
              SizedBox(height: 20,),
              Row(
                children: [
                  Expanded(
                      flex: 3,
                      child: Center(
                          child: IconButton(
                        icon: Image.asset('assets/images/walker50.png'),
                        iconSize: 50,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => WalkersPage()),
                          );
                        },
                      ))),
                  Expanded(
                    flex: 4,
                    child: Text(
                      "Paseadores",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 30.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  )
                ],
              ),
              SizedBox(height: 20,),
              Row(
                children: [
                  Expanded(
                      flex: 3,
                      child: Center(
                          child: IconButton(
                            icon: Image.asset('assets/images/soap50.png'),
                            iconSize: 50,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => GroomingPage()),
                              );
                            },
                          ))),
                  Expanded(
                    flex: 4,
                    child: Text(
                      "Baño",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 30.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  )
                ],
              ),
              SizedBox(height: 20,),
                  Row(
                    children: [
                      Expanded(
                          flex: 3,
                          child: Center(
                              child: IconButton(
                                icon: Image.asset('assets/images/vet50.png'),
                                iconSize: 50,
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Vets()),
                                    //MaterialPageRoute(builder: (context) => listAgenda()),
                                  );
                                },
                              ))),
                      Expanded(
                        flex: 4,
                        child: Text(
                          "Veterinario",
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.8),
                              fontSize: 30.0,
                              height: 1.4,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ],
                  ),
              SizedBox(height: 20,),
              Row(
                children: [
                  Expanded(
                      flex: 3,
                      child: Center(
                        child: IconButton(
                          icon: Image.asset('assets/images/appointments50.png'),
                          iconSize: 50,
                          color: Colors.green,
                          splashColor: Colors.purple,
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => listAgenda()),
                              //MaterialPageRoute(builder: (context) => listAgenda()),  
                            );
                          },
                        ),
                      )),
                  Expanded(
                    flex: 4,
                    child: Text(
                      "Agenda",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 30.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20,),
              Row(
                children: [
                  Expanded(
                      flex: 3,
                      child: Center(
                        child: IconButton(
                          icon: Image.asset('assets/images/man50.png'),
                          iconSize: 50,
                          color: Colors.green,
                          splashColor: Colors.purple,
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => AdoptionPetsPage()),
                            );
                          },
                        ),
                      )),
                  Expanded(
                    flex: 4,
                    child: Text(
                      "Adopcion",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 30.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doge50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/man50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/cart48.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
              label: '',
            ),
          ],
        ),
      ),
    );
  }
}
