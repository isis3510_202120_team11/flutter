import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pets/services/auth.dart';
import 'package:flutter_pets/start.dart';
import 'home.dart';
import 'login_config/Header.dart';
import 'login_config/InputWrapper.dart';
import 'login_config/button.dart';
import 'login_config/inputField.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final AuthService _auth = AuthService();

  String _email = "";
  String _password = "";

  Widget _buildEmail(){
    return TextFormField(
      decoration: InputDecoration(labelText: "Email"),
      validator: (value){
        if(value == null || value.isEmpty){
          return "El correo es requerido";
        }
        return null;
      },
      onSaved: (value){
        _email = value!;
      },
    );
  }


  Widget _buildPassword(){
    return TextFormField(
      decoration: InputDecoration(labelText: "Contraseña"),
      validator: (value){
        if(value == null || value.isEmpty){
          return "La constraseña es requerida";
        }
        return null;
      },
      onSaved: (value){
        _password = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  colors: [Colors.white, Colors.white]),
              image: DecorationImage(
                image: AssetImage('assets/images/Screen.png'),
                fit: BoxFit.fill,
              )),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 180,
              ),
              Header(),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(60),
                      topRight: Radius.circular(60),
                    )),
                child: Padding(
                  padding: EdgeInsets.all(40),
                  child: Column(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Email',
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black)),
                              child: TextField(
                                decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.email_outlined),
                                    hintStyle: TextStyle(color: Colors.black),
                                    hintText: " Ingrese su correo electronico",
                                    border: OutlineInputBorder(),
                                ),
                                keyboardType: TextInputType.emailAddress,
                              ),
                            ),
                            Container(
                              alignment: Alignment.topLeft,
                              child: Text(
                                'Contraseña',
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    height: 2,
                                    fontSize: 20),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black)),
                              child: TextField(
                                decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.password_outlined),
                                  hintStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(),
                                  hintText: "Ingrese su contraseña",
                                ),
                                obscureText: true,
                                enableSuggestions: false,
                                autocorrect: false,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        "",
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(100),
                          ),
                          child: ButtonTheme(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                ElevatedButton(
                                  child: Text('Login'),
                                  style: ElevatedButton.styleFrom(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 65, vertical: 20),
                                      primary: Colors.yellow,
                                      onPrimary: Colors.black,
                                      onSurface: Colors.grey,
                                      textStyle: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                  onPressed: () {
                                    FirebaseFirestore.instance
                                        .collection('testing')
                                        .add({
                                      'timestamp':Timestamp.fromDate(DateTime.now()),
                                      'email': 'prueba_flutter@gmail.com',
                                      'password': 'prueba1234'
                                    });
                                    /*async {
                                  dynamic result = await _auth.signInAnon();
                                  if (result == null) {
                                    print('error signing in');
                                  } else {
                                    print('signed in');
                                    print(result);
                                  }*/
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Home()),
                                    );
                                  },
                                ),
                                SizedBox(
                                  height: 40,
                                ),
                                ElevatedButton(
                                  child: Text('Regresar'),
                                  style: ElevatedButton.styleFrom(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 50, vertical: 20),
                                      primary: Colors.blue,
                                      onPrimary: Colors.white,
                                      onSurface: Colors.grey,
                                      textStyle: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Start()),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ))
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}


/*class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                colors: [Colors.white, Colors.white]),
            image: DecorationImage(
              image: AssetImage('assets/images/Screen.png'),
            )),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 200,
            ),
            Header(),
            Expanded(
                child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                  )),
              child: InputWrapper(),
            ))
          ],
        ),
      ),
    );
  }
}
*/