import 'package:flutter/material.dart';
import 'register.dart';
import 'login.dart';

//void main() => runApp(Start());

class Start extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          child: Column(
            children: [
              const SizedBox(
                height: 50,
              ),
              Container(
                  child: Image.asset(
                'assets/images/Logo.jpg',
                height: 400,
                width: 2500,
              )),
              const SizedBox(
                height: 50,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 50, vertical: 20),
                    primary: Colors.yellow,
                    onPrimary: Colors.black,
                    onSurface: Colors.grey,
                    textStyle: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold)),
                child: Text(
                  'Login',
                  style: TextStyle(fontSize: 20),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Login()),
                  );
                },
              ),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                child: Text(
                  'Registrarse',
                  style: TextStyle(fontSize: 20),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Register()),
                  );
                },
                style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 50, vertical: 20),
                    primary: Colors.blue,
                    onPrimary: Colors.black,
                    onSurface: Colors.grey,
                    textStyle: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
