import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
// Import the firebase_core plugin
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:geolocator/geolocator.dart';
import 'home.dart';


//void main() => runApp(MyApp());

class WalkersPage extends StatefulWidget {
  @override
  _ListWalkersState createState() => _ListWalkersState();
}

class _ListWalkersState extends State<WalkersPage> {
  bool _initialized = false;
  bool _error = false;

  Position? _currentUserPosition;
  int? distanceInKm = 0;

  Future _getDistance(double lat, double lon)async{
    _currentUserPosition = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    double distanceInMeter = await Geolocator.distanceBetween(_currentUserPosition!.latitude, _currentUserPosition!.longitude, lat, lon);
    distanceInKm = (distanceInMeter!/1000)!.round();
  }

  void initWalkers(){
    if(_initialized){
      walkers =  FirebaseFirestore.instance.collection("walkers2");
    }
  }
  // Define an async function to initialize FlutterFire
  void initializeFlutterFire() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
      await Firebase.initializeApp();
      setState(() {
        _initialized = true;
        initWalkers();
      });
    } catch(e) {
      // Set `_error` state to true if Firebase initialization fails
      setState(() {
        _error = true;
      });
    }
  }

  static final customCacheManager = CacheManager(
    Config(
      'customCacheKey',
      stalePeriod: Duration(days:1),
      maxNrOfCacheObjects: 10,
    ),
  );

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }
  late CollectionReference walkers;




  @override
  Widget build(BuildContext context) {
    // Show error message if initialization failed
    if (_error) {
      return MaterialApp(
        home: Scaffold(
          body: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/Screen.png'),
                )),
            padding: EdgeInsets.only(top: 50.0),
            child: Column(
              children: [
                Container(
                  margin: new EdgeInsets.symmetric(horizontal: 20.0),
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                    child: Text('Regresar'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Home()),
                      );
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.blue),
                      textStyle: MaterialStateProperty.all(
                        TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                        flex: 3,
                        child: Center(
                          child: Image.asset('assets/images/man100.png'),
                        )),
                    Expanded(
                      flex: 4,
                      child: Text(
                        "Adopt",
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.8),
                            fontSize: 38.0,
                            height: 1.4,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Column(
                    children: [
                      Text("Error!")
                    ],
                  ),
                )
              ],
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/doge50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/man50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/cart48.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
                label: '',
              ),
            ],
          ),
        ),
      );
    }

    // Show a loader until FlutterFire is initialized
    if (!_initialized) {
      return MaterialApp(
        home: Scaffold(
          body: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/Screen.png'),
                )),
            padding: EdgeInsets.only(top: 50.0),
            child: Column(
              children: [
                Container(
                  margin: new EdgeInsets.symmetric(horizontal: 20.0),
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                    child: Text('Regresar'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Home()),
                      );
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.blue),
                      textStyle: MaterialStateProperty.all(
                        TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                        flex: 3,
                        child: Center(
                          child: Image.asset('assets/images/man100.png'),
                        )),
                    Expanded(
                      flex: 4,
                      child: Text(
                        "Adopt",
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.8),
                            fontSize: 38.0,
                            height: 1.4,
                            fontWeight: FontWeight.w600),
                        textAlign: TextAlign.left,
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(40.0),
                  child: Column(
                    children: [
                      Text("Cargando!")
                    ],
                  ),
                )
              ],
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/doge50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/man50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/cart48.png')),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
                label: '',
              ),
            ],
          ),
        ),
      );
    }
    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/Screen.png'),
              )),
          padding: EdgeInsets.only(top: 50.0),
          child: Column(
            children: [
              Container(
                margin: new EdgeInsets.symmetric(horizontal: 20.0),
                alignment: Alignment.topRight,
                child:
                ElevatedButton(
                  child: Text('Regresar'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home()),
                    );
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.blue),
                    textStyle: MaterialStateProperty.all(
                      TextStyle(fontSize: 15),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      flex: 3,
                      child: Center(
                        child: Image.asset('assets/images/walker100.png'),
                      )
                  ),
                  Expanded(
                    flex: 4,
                    child: Text(
                      "Walkers",
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 38.0,
                          height: 1.4,
                          fontWeight: FontWeight.w600),
                      textAlign: TextAlign.left,
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(40.0),
                child: Column(
                  children: [
                    FutureBuilder(
                      future: walkers.get(),
                      builder: (_, AsyncSnapshot<dynamic> snapshot){
                        if(snapshot.connectionState == ConnectionState.waiting){
                          return Center(
                            child: Text("Loading..."),
                          );
                        }
                        else if(snapshot.connectionState == ConnectionState.none){
                          return Center(
                            child: Text("Not connected"),
                          );
                        }
                        else{
                          return ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: snapshot.data.docs.length,
                              itemBuilder: (_, index){
                                _getDistance(snapshot.data.docs[index]["location"].latitude, snapshot.data.docs[index]["location"].longitude);
                                //print(distanceInKm);
                                if(distanceInKm! <= 6078) {
                                  return Row(
                                    children: [
                                      Expanded(
                                        flex: 3,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          children: [
                                            CachedNetworkImage(
                                                key: UniqueKey(),
                                                cacheManager: customCacheManager,
                                                imageUrl: snapshot.data.docs[index]["img"],
                                                placeholder: (context, url) => const CircularProgressIndicator(),
                                                errorWidget: (context, url, error) => Container(
                                                  color: Colors.black12,
                                                  child: Icon(Icons.error, color: Colors.red),
                                                ),
                                                ),
                                          ],

                                        ),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    "Nombre:",
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.bold
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: Text(
                                                      snapshot.data.docs[index]["name"]
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 7,),
                                            Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    "Rating:",
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.bold
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: Text(
                                                      snapshot.data.docs[index]["rating"].toString()
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 7,),
                                            Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    "Costo:",
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.bold
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: Text(
                                                      snapshot.data.docs[index]["price"].toString()
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 7,),
                                            Row(
                                              children: [
                                                Expanded(
                                                  flex: 1,
                                                  child: Text(
                                                    "Distancia:",
                                                    style: TextStyle(
                                                        fontWeight: FontWeight.bold
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: FutureBuilder(
                                                      future: _getDistance(snapshot.data.docs[index]["location"].latitude, snapshot.data.docs[index]["location"].longitude),
                                                      builder: (_, AsyncSnapshot<dynamic> snapshot) {
                                                        return Text(
                                                            distanceInKm.toString() + " KM"

                                                        );
                                                      }
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 50,),
                                    ],
                                  );
                                  }
                                else{
                                  return SizedBox(height: 1,);
                                }

                          });
                        }
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doge50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/man50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/cart48.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
              label: '',
            ),
          ],
        ),
      ),
    );
  }
}
