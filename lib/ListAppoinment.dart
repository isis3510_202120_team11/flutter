// main.dart
import 'package:flutter/material.dart';

void main() {
  runApp(listAgenda());
}

class listAgenda extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Agenda'),
        ),
        body:
        Column(
          children: [
            SizedBox(height: 15,),
            ListTile(
              leading: Icon(Icons.auto_stories_outlined),
              title: Text('Cita paseador'),
              trailing: Icon(Icons.done),
              subtitle: Text('Nombre: Marco Polo                                  Hora: 3:00 PM                                                    Fecha: 14/11/2021'
              ),
              onTap: () { },
            ),
            SizedBox(height: 15,),
            ListTile(
              leading: Icon(Icons.auto_stories_outlined),
              title: Text('Cita paseador'),
              trailing: Icon(Icons.done),
              subtitle: Text('Nombre: Marco Polo                                  Hora: 4:00 PM                                                    Fecha: 14/11/2021'
              ),
              onTap: () { },
            ),
            SizedBox(height: 15,),
            ListTile(
              leading: Icon(Icons.auto_stories_outlined),
              title: Text('Cita veterinario'),
              trailing: Icon(Icons.done),
              subtitle: Text('Nombre: Jairo Gonzalez                                 Hora: 7:00 PM                                                    Fecha: 14/11/2021'
              ),
              onTap: () { },
            ),
          ],

        )
    );
  }
}
