import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pets/schedule.dart';

import 'home.dart';

//void main() => runApp(MyApp());

import 'package:flutter/material.dart';
import 'package:flutter_pets/schedule.dart';

import 'home.dart';

//void main() => runApp(MyApp());

class appointments extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Paseadores'),
        ),
        body: StreamBuilder(
          stream:
              FirebaseFirestore.instance.collection('Appointment').snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) return new Text('Loading...');
            final archives = snapshot.data!.docs;
            return new ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                return new ListTile(
                  title: new Text('Cita 1'),
                  subtitle: new Text(document['client'] +
                      document['fecha'] +
                      document['hora'] +
                      document['petName']),
                );
              }).toList(),
            );
          },
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doge50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/man50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/doghouse50.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/cart48.png')),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(AssetImage('assets/images/appointments50.png')),
              label: '',
            ),
          ],
        ),
      ),
    );
  }
}
